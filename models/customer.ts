module.exports = (sequelize: any, DataTypes: any) => {
  const Customer = sequelize.define(
    "customer",
    {
      name: {
        type: DataTypes.STRING,
      },
      doorno: {
        type: DataTypes.STRING,
      },
      street: {
        type: DataTypes.STRING,
      },
      place: {
        type: DataTypes.STRING,
      },
      pin: {
        type: DataTypes.STRING,
      },
      state_id: {
        type: DataTypes.BIGINT,
      },
      coluntry_id: {
        type: DataTypes.BIGINT,
      },
    },
    {
      timestamps: true,
    }
  );
  return Customer;
};
