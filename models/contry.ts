module.exports = (sequelize: any, DataTypes: any) => {
  // console.log("Triggred Country Model");
  const Contry = sequelize.define(
    "contry",
    {
      name: {
        type: DataTypes.STRING,
        unique: true,
      },
    },
    {
      timestamps: true,
    }
  );
  return Contry;
};
