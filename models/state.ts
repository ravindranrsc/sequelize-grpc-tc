module.exports=(sequelize:any,DataTypes:any)=>{
const State= sequelize.define(
    'state',{
        name:{
            type:DataTypes.STRING,
            unique: true,
        }

},{
    timestamps:true
});
return State;
}